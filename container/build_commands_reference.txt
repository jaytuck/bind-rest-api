# to build the container
docker build container --tag jaytuckey/bind-rest-api:latest --tag jaytuckey/bind-rest-api:v1.4.1

# run the container
docker run --volume (realpath ./apikeys.pass):/app/apikeys.pass --volume (pwd)/logs:/app/logs --env-file config.env --publish 8000:8000 jaytuckey/bind-rest-api:latest

# push the container to docker hub
docker push jaytuckey/bind-rest-api:latest
docker push jaytuckey/bind-rest-api:v1.4.1
