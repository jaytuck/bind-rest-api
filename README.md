# bind-rest-api

This is a BIND API that allows the following functionality:
* dumping the zone file to JSON
* GETting a specific DNS record
* POSTing to create a new DNS record
* PUTing to replace a DNS record
* DELETEing a DNS record

This project is inspired by [https://github.com/dmyerscough/BIND-RESTful](https://github.com/dmyerscough/BIND-RESTful)

But it adds extra functionality, including:
* using [FastAPI](https://fastapi.tiangolo.com/) instead of Flask as the framwork, which allows:
  * auto-generated API docs and tooling
  * full input validation
* API Key protection (but still definitely don't expose this to the internet)
* Audit log of `apikey` ➡ `DNS changes`
* [acme.sh](https://acme.sh) tooling to generate LetsEncrypt certificates using the API
* Docker container to make it easier to run: https://hub.docker.com/r/jaytuckey/bind-rest-api

## Auto-generated docs

By using FastAPI this project get's auto-generated Swagger-UI docs:
![auto docs 1](img/bind-rest-api-01.png)

![auto docs 2](img/bind-rest-api-02.png)

## Getting set up and running

I have made a video of myself setting up the project: https://youtu.be/ZNEtmWhu1HI

* Make sure you have python 3 and [poetry](https://python-poetry.org/) installed.
* From within the folder, run `poetry install` to install all the required dependencies.
  * Poetry will try to install into a virtualenv
  * You can activate the auto-created virtualenv with `poetry shell`
  * Once actived you should see the dependencies with `pip list`. Make sure you see lines like:
  ```
  dnspython         2.4.2
  fastapi           0.109.0
  ```
* copy the `example_config.env` and `example_apikeys.pass` files to `config.env` and `apikeys.pass`
* Customise the files and set the values you need. Make sure to generate very long api keys.
* export all the environment variables in `config.env` - (try `export $(cat config.env)` in bash)
* run the api with uvicorn - `uvicorn bindapi:app`

### Keys and Flow
There are two flows that need keys:

`HTTP Clients --> API` and `API --> bind9`

#### `HTTP Clients --> API`
These clients use an `X-Api-Key` HTTP header when performing requests. The API confirms that the key provided is in the `apikeys.pass` file, and then logs the friendly name for that api key.

#### `API --> bind9`
To send updates to the bind9 server the API uses a TSIG key. You put this TSIG username/password in the `config.env` file, and then also reference it in the bind9 configuration. For full details see the bind9 docs: https://bind9.readthedocs.io/en/stable/chapter7.html#tsig

To test your TSIG key you can use the `nsupdate` tool: https://bind9.readthedocs.io/en/stable/manpages.html#nsupdate-dynamic-dns-update-utility

Setup instructions may differ from this, but this is what I have on Ubuntu 22.04:
```
# Create the key (if needed)
root@bindify:/etc/bind# tsig-keygen api-tsig-key > /etc/bind/api-tsig-key.key

# First check the api key is in the key file
root@bindify:/etc/bind# cat /etc/bind/api-tsig-key.key 
key "api-tsig-key" {
   algorithm hmac-sha256;
   secret "RwX...+A=";
};

# I've then included this file in the /etc/bind/named.conf.local file:
root@bindify:/etc/bind# grep -C1 'API tsig' /etc/bind/named.conf.local

// API tsig key
include "/etc/bind/api-tsig-key.key";

# Then, in my zone configuration, I've allowed this key to send updates:
root@bindify:/etc/bind# grep -A2 'h.jaytuckey.name' /etc/bind/named.conf.local
zone "h.jaytuckey.name" IN {
    type master;
    file "/var/lib/bind/h.jaytuckey.name.zonefile";
    update-policy { grant api-tsig-key zonesub any; };
};

# Now test the key by creating a record

# Verify the record doesn't exist
root@bindify:~# host test-tsig-key.h.jaytuckey.name localhost
Using domain server:
Name: localhost
Address: ::1#53
Aliases: 

Host test-tsig-key.h.jaytuckey.name not found: 3(NXDOMAIN)

# Create the key
root@bindify:/etc/bind# echo '
server 127.0.0.1
update add test-tsig-key.h.jaytuckey.name 86400 A 172.16.1.1
send
' | nsupdate -k /etc/bind/api-tsig-key.key

# Example of trying to create the record with a bad key:
root@bindify:/etc/bind# echo '
server 127.0.0.1
update add test-tsig-key.h.jaytuckey.name 86400 A 172.16.1.1
send
' | nsupdate -k /etc/bind/api-tsig-key-badkey.key 
; TSIG error with server: tsig indicates error
update failed: NOTAUTH(BADSIG)

# After creating, verify record is there:
root@bindify:/etc/bind# host test-tsig-key.h.jaytuckey.name localhost
Using domain server:
Name: localhost
Address: ::1#53
Aliases: 

test-tsig-key.h.jaytuckey.name has address 172.16.1.1

# Clean up the key
root@bindify:/etc/bind# echo '
server 127.0.0.1
update delete test-tsig-key.h.jaytuckey.name A
send
' | nsupdate -k /etc/bind/api-tsig-key.key -L 1

# and verify deletion
root@bindify:/etc/bind# host test-tsig-key.h.jaytuckey.name localhost
Using domain server:
Name: localhost
Address: ::1#53
Aliases: 

Host test-tsig-key.h.jaytuckey.name not found: 3(NXDOMAIN)
```
