#!/usr/bin/fish

# Config
set API_KEY_FILE apikeys.pass
set API_KEY_NAME testkey
set API_KEY (grep $API_KEY_NAME, $API_KEY_FILE | string split ,)[2]


function curlit
	echo -e "\ncurling URL $argv[-1]"
	echo -e "curling with method $argv[1..3]"
	$argv 2>| read -z err
	set CURLSTATUS $status
	echo -e "\n"
	echo $err | grep '< HTTP'
	echo -e "\ncurl exited with status $CURLSTATUS"
end


set curlget curl -X 'GET' \
  -H 'accept: application/json' \
  -H 'X-Api-Key: '$API_KEY \
  -v

set curlput curl -X 'PUT' \
  -H 'accept: application/json' \
  -H 'X-Api-Key: '$API_KEY \
  -H 'Content-Type: application/json' \
  -v

set curlpost curl -X 'POST' \
  -H 'accept: application/json' \
  -H 'X-Api-Key: 8rb91EBsxQvt7CWijNzrPGlqQM2ZVNPHv5dS/5QZS0M=' \
  -H 'Content-Type: application/json' \
  -v

set curldelete curl -X 'DELETE' \
  -H 'accept: application/json' \
  -H 'X-Api-Key: 8rb91EBsxQvt7CWijNzrPGlqQM2ZVNPHv5dS/5QZS0M=' \
  -H 'Content-Type: application/json' \
  -v


curlit $curlget 'http://127.0.0.1:8000/dns/zone/h.jaytuckey.name'
curlit $curlget 'http://127.0.0.1:8000/dns/zone/z.h.jaytuckey.name'
echo "400 error is expected on this request"
curlit $curlget 'http://127.0.0.1:8000/dns/record/testrecord.h.jaytuckey.name?record_type=A'
curlit $curlput   -d '{
  "response": "127.0.0.99",
  "rrtype": "A",
  "ttl": 3600
}' 'http://127.0.0.1:8000/dns/record/testrecord.h.jaytuckey.name'
curlit $curlget 'http://127.0.0.1:8000/dns/record/testrecord.h.jaytuckey.name?record_type=A'
curlit $curlpost -d '{
  "response": "10.9.1.135",
  "rrtype": "A",
  "ttl": 3600
}' 'http://127.0.0.1:8000/dns/record/testrecord.h.jaytuckey.name'
curlit $curlget 'http://127.0.0.1:8000/dns/record/testrecord.h.jaytuckey.name?record_type=A'
curlit $curldelete -d '{
  "response": "10.9.1.135",
  "rrtype": "A",
  "ttl": 3600
}' 'http://127.0.0.1:8000/dns/record/testrecord.h.jaytuckey.name'
curlit $curlget 'http://127.0.0.1:8000/dns/record/testrecord.h.jaytuckey.name?record_type=A'
curlit $curldelete 'http://127.0.0.1:8000/dns/allrecords/testrecord.h.jaytuckey.name?recordtypes=A&recordtypes=AAAA&recordtypes=CNAME&recordtypes=MX&recordtypes=NS&recordtypes=TXT&recordtypes=SOA&recordtypes=PTR'
curlit $curlget 'http://127.0.0.1:8000/dns/record/testrecord.h.jaytuckey.name?record_type=A'
